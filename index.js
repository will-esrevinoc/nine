var express = require('express');
var handle = require('./handle');

var app = express();

app.use(express.json());
app.use(function(err, req, res, next) {
  res.status(400).json(
    { error: 'Could not decode request: JSON parsing failed' }
  );
});

app.post('/', function (req, res) {
  try {
    res.status(200).json(handle.process(req.body));
  } catch (e) {
    res.status(400).json({ error: e.message });
  }
});

app.listen(process.env.PORT || 80);
