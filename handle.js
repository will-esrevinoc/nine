function payloadMustExistAndValid(body) {
  if (!('payload' in body) || !Array.isArray(body.payload)) {
    throw new Error('key `payload` not found or invalid');
  }
}

function hasValidImage(entry) {
  return 'image' in entry &&
    typeof entry.image === 'object' &&
    entry.image !== null &&
    'showImage' in entry.image &&
    typeof entry.image.showImage === 'string' &&
    entry.image.showImage !== '';
}

function hasValidSlug(entry) {
  return 'slug' in entry && typeof entry.slug === 'string';
}

function hasValidTitle(entry) {
  return 'title' in entry && typeof entry.title === 'string';
}

function hasValidDrm(entry) {
  return 'drm' in entry && entry.drm === true;
}

function hasValidEpisodeCount(entry) {
  return 'episodeCount' in entry && Number(entry.episodeCount) > 0;
}

function isWantedEntry(entry) {
  return typeof entry === 'object' &&
    entry !== null &&
    hasValidDrm(entry) &&
    hasValidEpisodeCount(entry) &&
    hasValidImage(entry) &&
    hasValidSlug(entry) &&
    hasValidTitle(entry);
}

function transform(entry) {
  return {
    image: entry.image.showImage,
    slug: entry.slug,
    title: entry.title
  };
}

exports.process = function (body) {
  payloadMustExistAndValid(body);
  return {
    response: body.payload.filter(isWantedEntry).map(transform)
  };
}

exports._isWantedEntry = isWantedEntry;
exports._payloadMustExistAndValid = payloadMustExistAndValid;
