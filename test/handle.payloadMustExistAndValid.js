var expect = require('chai').expect;
var payloadMustExistAndValid = require('../handle')._payloadMustExistAndValid;

describe('payloadMustExistAndValid()', function() {
  it('should throw no exception', function() {
    expect(() => payloadMustExistAndValid({
      payload: []
    })).to.not.throw();
  });

  it('should throw an exception', function() {
    expect(() => payloadMustExistAndValid({})).to.throw();
    expect(() => payloadMustExistAndValid({
      payload: {}
    })).to.throw();
  });
});
