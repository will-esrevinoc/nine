var expect = require('chai').expect;
var isWantedEntry = require('../handle')._isWantedEntry;

var validEntry = {
  drm: true,
  episodeCount: 3,
  image: {
    showImage: 'someimage'
  },
  slug: 'slug',
  title: 'title'
};

describe('isWantedEntry() -> true', function() {
  it('should return true', function() {
    expect(isWantedEntry(validEntry)).to.be.true;
  });
});

function deleteProp(prop) {
  var copy = { ...validEntry };
  delete copy[prop];
  return copy;
}

describe('isWantedEntry() -> false', function() {
  var data = [
    {
      reason: 'invalid entry - not an object',
      entry: 2
    },
    {
      reason: 'invalid entry - null',
      entry: null
    },

    // drm
    {
      reason: 'drm is missing',
      entry: deleteProp('drm')
    },
    {
      reason: 'drm is false',
      entry: { ...validEntry, drm: false }
    },
    {
      reason: 'drm is invalid',
      entry: { ...validEntry, drm: 2 }
    },

    // episode
    {
      reason: 'episodeCount is missing',
      entry: deleteProp('episodeCount')
    },
    {
      reason: 'episodeCount is 0',
      entry: { ...validEntry, episodeCount: 0 }
    },
    {
      reason: 'episodeCount is invalid',
      entry: { ...validEntry, episodeCount: 'a' }
    }

    /**
     * The rest should be similar
     *
     * image
     * image/showImage
     * slug
     * title
     */
  ];

  data.forEach((value) => {
    it (value.reason, function() {
      expect(isWantedEntry(value.entry)).to.be.false;
    });
  });
});
